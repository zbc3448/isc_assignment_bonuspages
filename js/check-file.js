function fileChange(input, text) {
    var inputE = document.getElementById(input);
    var textE = document.getElementById(text);
    inputE.style.backgroundColor = '#4169E1';
    inputE.style.border = '2px solid #4169E1';
    if (textE.innerHTML == null || textE.innerHTML == "") {
        textE.textContent += "File added";
    }
    textE.style.color = '#4169E1';
}

function selectCheckBox(cbx, div, text1, text2) {
    var cbxE = document.getElementById(cbx);
    var divE = document.getElementById(div);
    var text1E = document.getElementById(text1);
    var text2E = document.getElementById(text2);
    if (cbxE.checked) {
        divE.style.backgroundColor = '#4169E1';
        divE.style.border = '2px solid #4169E1';
        if (text1E.innerHTML == null || text1E.innerHTML == "") {
            text1E.textContent += "Yes";
        }
        text1E.style.color = '#4169E1';
        text2E.style.color = "#fff";
    } else {
        divE.style.backgroundColor = '#fff';
        divE.style.border = '2px solid #aaa';
        if (text1E.innerHTML == null || text1E.innerHTML == "") {
            text1E.textContent = '';
        }
        text1E.style.color = '#fff';
        text2E.style.color = "#000";
    }
}